package Mocks;


public class PageRequest{
	
	public String path;
	public String json = ""; // this is the final json
	
	public ResponseDataType type;
	
	public String filled_json ="";
	public String empty_json ="";  // all the structure but with zero-length arrays
	public String broken_json =""; 
	public String untouched_json =""; //
	
	public PageRequest(String path){
		this.path = path;
	}

	public void setMock(ResponseDataType type) {
		this.type = type;
		switch(type) {
			 case FILLED:  json = filled_json; break;
			 case EMPTY:  json =  empty_json; break;
			 case BROKEN:  json =  broken_json; break;
			 case UNTOUCHED:  json =  untouched_json; break;
			 
			 default: json =  untouched_json;

		}
		//System.out.println("mock="+type+"\tjson="+json);
	}

	
}