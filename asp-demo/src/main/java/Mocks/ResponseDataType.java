package Mocks;

public enum ResponseDataType {
	/**************************************************************
	 * FILLED - it means json always will be same with content to display, but not astronomical (wink!)
	 * EMPTY - json will contain basic structure but without any data. this needs to check 'No data' message
	 * BROKEN - broken json scructure to raise validation errors
	 * UNTOUCHED - it is untouched json right from the server without any modification
	 *************************************************************/
	
	/**
	 * FILLED - it means json always will be the same with content to display, but not astronomical (wink!)
	 */
	FILLED,
	/**
	 * EMPTY - json will contain basic structure but without any data. this needs to check 'No data' message
	 */
	EMPTY,
	/**
	 * BROKEN - broken json scructure to raise valudation errors
	 */
	BROKEN,
	/**
	 * UNTOUCHED - it is untouched json right from the server without any modification
	 */
	UNTOUCHED
	/*
	public enum Types {
	    FILLED, EMPTY, BROKEN, UNTOUCHED
	}*/

}
