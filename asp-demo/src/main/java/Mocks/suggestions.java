package Mocks;

import java.util.Arrays;
import java.util.List;


import driver_utils.Tools;
import driver_utils.test_properties;


public class suggestions {


	
	
	
	public PageRequest suggestions_json = new PageRequest(suggestions.data.path); 


										
	public List<PageRequest> getAllRequests(){
		return Arrays.asList(
				suggestions_json);
											
	}
	
	public void setMockForAllRequests(ResponseDataType type) {
		suggestions_json.filled_json =suggestions.data.Filled();
		suggestions_json.broken_json =suggestions.data.Broken();
		suggestions_json.untouched_json ="";
		suggestions_json.setMock(type);

	}
	
// all static below	

	public static String path = "";
	

	
	
	
	public static class data {
		
		public static String path = suggestions.path+"/apex_suggestions";
		
		
		public static String Broken() {
			String json = "{}";
			return json;
		}
		
		
		
		public static String Filled() {
			return Tools.ReadJSON(test_properties.Get_Mock_Folder_Path()+"suggestions.json");
		}



	}
	
	
	
}
