package Mocks;

import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import net.lightbody.bmp.filters.ResponseFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

public class ResponseFilterBuilder {
/*
 * TODO
 * need to use regular expressions instead of '.contains(urlpath)'
 */
	public static ResponseFilter getMockResponse(String json, String urlpath) {
	return 
			new ResponseFilter() {
			        @Override
			        public void filterResponse(HttpResponse response, HttpMessageContents contents, HttpMessageInfo messageInfo) {
						try {
							
							
							if (messageInfo.getOriginalUrl().toLowerCase().contains(urlpath)&&(messageInfo.getOriginalRequest().getMethod().equals(HttpMethod.GET))) {
			
								response.headers().remove("date");
				            	response.headers().remove(HttpHeaders.Names.CONTENT_ENCODING);
				            	response.headers().remove(HttpHeaders.Names.TRANSFER_ENCODING);
				            	response.headers().remove(HttpHeaders.Names.CONTENT_TYPE);
				            	response.headers().add(HttpHeaders.Names.CONTENT_TYPE, "application/json; charset=utf-8");
			
				            	if (response.getStatus().equals(HttpResponseStatus.NOT_FOUND))
				            		response.setStatus(HttpResponseStatus.OK);
				            	
				            	if ( ! json.isBlank())
				            		contents.setTextContents(json);
				                
				            }
							}catch(Exception e) {
								System.out.println("Filter error:"+e);
							}
			        }
			    };
	}
	
}
