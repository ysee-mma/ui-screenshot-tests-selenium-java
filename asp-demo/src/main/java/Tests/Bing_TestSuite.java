package Tests;


import org.junit.Test;


import driver_utils.TestSuit;

public class Bing_TestSuite  extends TestSuit {
	
	
	@Test
	public void Test_Page_Screen() {
			
		String testname = "Test_Page_Screen";
		
			System.out.println("=================== Bing Help - Page screen test ================");
			
			PageObjects.Bing_Help bing_help = new PageObjects.Bing_Help(); 
			
			bing_help.Open();
			
			bing_help.Second_Dropdown.Click(driver);
			
			bing_help.page.Check(driver, testname);
			
	}


}
