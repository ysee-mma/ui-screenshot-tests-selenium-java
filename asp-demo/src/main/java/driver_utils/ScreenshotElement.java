package driver_utils;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;


public class ScreenshotElement {
	
	public static <T> T target(By by, long timeout_extra, OutputType<T> type) {
	    return ((TakesScreenshot) driver_wrapper.waitForElement(by, timeout_extra))
	            .getScreenshotAs(type);
	}

	public static <T> T raw(WebElement el, OutputType<T> type) {
	    return ((TakesScreenshot) el)
	            .getScreenshotAs(type);
	}
	
	
	public static BufferedImage raw (WebElement el) {
	    try {
			return ImageIO.read(((TakesScreenshot) el)
			        .getScreenshotAs(OutputType.FILE));
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return null;
	}


	
	public static BufferedImage cleared(WebElement el, List<WebElement> ignore_list) {
		
		
		try {
			
			BufferedImage fullImg1 = ImageIO.read(((TakesScreenshot) el)
			        .getScreenshotAs(OutputType.FILE));
			
			
			for (WebElement ignore_item:ignore_list) {
				Rectangle res =  ignore_item.getRect();
				
				Graphics2D graph = fullImg1.createGraphics();
		        graph.setColor(Color.YELLOW);
		        graph.fillRect(res.x-el.getRect().x, res.y-el.getRect().y, res.width, res.height);
		        graph.dispose();
			}


	        return fullImg1;
			
			
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
				
	    return null;
	} 
	



}
