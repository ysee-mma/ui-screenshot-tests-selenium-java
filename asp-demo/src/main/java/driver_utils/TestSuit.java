package driver_utils;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class TestSuit  {
	
	public static WebDriver driver;
	WebElement element;
	

	

	@BeforeClass
	public static void openBrowser(){
		
		driver = driver_wrapper.OpenBrowser();
		
	}
	
	@AfterClass
	public static void closeBrowser(){
		
		driver_wrapper.CloseBrowser();
	}
	
	@After
	public  void restartProxy(){
		
		driver_wrapper.StopAndStartProxy();
	}

	@Before
	public  void setTimestamp(){

		SimpleDateFormat dateFormat = new SimpleDateFormat(test_properties.Get_TimeStampFormat());
		driver_wrapper.test_timeStamp  = dateFormat.format(new Date(System.currentTimeMillis()));
	}

}


