package driver_utils;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Tools {
		
	
	public static String ReadJSON(String Path) {
		String json = "";
		try {
			json = new String(Files.readAllBytes(Paths.get(Path)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;
	}
	

	
	
	public static String BaselinePathBlank( String componentName, String testname) {
		String browser = driver_wrapper.env_browser_for_test;
		return ".\\baseline\\"+componentName+"\\"+browser+"\\"+testname+"\\-1WHAT1-.png";
	}
		
	public static String BaselinePath( String componentName, String testname) {
		return BaselinePathBlank(componentName, testname).replace("-1WHAT1-","-baseline-");
	}
	
	public static String BaselineDIFFPath( String componentName, String testname) {
		return BaselinePathBlank(componentName, testname).replace("-1WHAT1-","-DIFFS-"+driver_wrapper.test_timeStamp);
	}
	
	public static String BaselineNEWPath( String componentName, String testname) {
		return BaselinePathBlank(componentName, testname).replace("-1WHAT1-","-NEW-"+driver_wrapper.test_timeStamp);
	}
}
