package driver_utils;


import org.openqa.selenium.Dimension;

public class test_properties {

	
	
	public static class browser_specs {

		public static Dimension getSize() {
			
			return new Dimension(1920, 1180);
			
		}
		
	}
	
	
	public static String Get_Mock_Folder_Path() {
			return ".\\src\\shr\\Mocks\\data\\";
		
	}
	
	public static String Get_TimeStampFormat() {
		return "dd-MM-yyyy_HH_mm_ss";
	
	}

}
