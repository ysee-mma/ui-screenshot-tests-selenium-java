package driver_utils;


import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;

import Mocks.PageRequest;

public class driver_wrapper {
	
	
	public static WebDriver driver;
	public static Proxy proxy;
	
	public static BrowserMobProxy bmproxy;
	static int port = 9996;
	
	
	private static long wait_element_sec_timeout = 30;
	
	public static String ChromeDriverFilePath = "chromedriver_v92.exe"; 
	
	
	private enum ENV_BROWSER {
	    chrome 
	}
	public static String env_browser_for_test = ENV_BROWSER.chrome.toString();
	
	public static String test_timeStamp = "";
	
	
	public static WebDriver OpenBrowser(){
		
        try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("browser", "Chrome");
	
			ChromeDriverService service = new ChromeDriverService.Builder()
			                            .usingDriverExecutable(new File(ChromeDriverFilePath))
			                            .usingAnyFreePort()
			                            .build();
			ChromeOptions options = new ChromeOptions();
			
			// https://chromium.googlesource.com/chromium/src/+/refs/heads/main/chrome/common/chrome_switches.cc
			options.addArguments("--disable-infobars");
			options.addArguments("--disable-notifications");
			//options.addArguments("--headless");
			options.addArguments("--incognito");
			options.addArguments("--no-default-browser-check");
			options.addArguments("--window-position=0,0");
			//options.addArguments("--window-size=1920,1180");
			
		
			
			//options.addArguments("--auto-open-devtools-for-tabs");
			 
	
			  LoggingPreferences logPrefs = new LoggingPreferences();
			  logPrefs.enable( LogType.PERFORMANCE, Level.ALL );
			  options.setCapability( "goog:loggingPrefs", logPrefs );
			
			ProxySetter.setProxy();
			options = ProxySetter.setSeleniumProxy(options);
			
			
			options.merge(capabilities);
			driver = new ChromeDriver(service, options);
			
		 
			//set window size
			driver.manage().window().setSize(test_properties.browser_specs.getSize());
			
			//((JavascriptExecutor) driver).executeScript("window.resizeTo(1920, 1080)");
			 
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
        }catch(Exception e) {
        	e.printStackTrace();
        }
		return driver;
	}
	
	
	public static void StopAndStartProxy() {
		//System.out.println("Proxy about to be restarted");
		bmproxy.stop();
		ProxySetter.setProxy();
		//System.out.println("Proxy restarted");
	}
	
	public static void CloseBrowser(){
		//closing the browser
		driver.close();
		driver.quit(); //Closing the driver once the tests are executed
	}
	
	public static WebElement waitForElement(By by, long timeout_sec_extra) {
		long wait = Math.max(timeout_sec_extra, wait_element_sec_timeout);
	    return new WebDriverWait(driver, wait)
	            .until(driver -> driver.findElement(by));
	}



	public static void WaitLoadComplete() {
		//System.out.println("state before= "+ ((JavascriptExecutor) driver).executeScript("return document.readyState"));
		new WebDriverWait(driver, 25000).until(
				driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
		//System.out.println("state after= "+ ((JavascriptExecutor) driver).executeScript("return document.readyState"));
	}
	
	public static void Go(String url) {
		
			driver.get(url);
			WaitLoadComplete();			
		
	}
	
	public static void GoAndWait(String path, List<String> requests_to_wait) {
		Go(path);
		Utils.WaitGETRequestsToComplete_har(driver,requests_to_wait,55000);
	}
	
	
	@SuppressWarnings("unchecked")
	public static void GoAndWait(String path, Object requests) {
		
		Go(path);
		
		if (requests == null)  return;
		
		if ((requests) instanceof List<?>){
			
			if (((List<?>)requests).get(0) instanceof PageRequest){
				List<String> paths = new ArrayList<String>();
				for(PageRequest req: (List<PageRequest>)requests) {
					paths.add(req.path);
				}
					Utils.WaitGETRequestsToComplete_har(
													driver,
													paths,
													55000);

			}
			
			if (((List<?>)requests).get(0) instanceof String){
				Utils.WaitGETRequestsToComplete_har(
												driver,
												(List<String>)requests,
												55000); 
			}
		}		
	}
	
	
	/********************************
	 * 	Utils to work
	 ********************************/
	public static class Utils{
		

		
		public static void WaitGETRequestsToComplete_har(WebDriver driver, List<String> request_paths_list, long timeout_ms) throws TimeoutException{
			//System.out.println("Request(s) to wait ["+String.join(", ", items)+"]");
			 long start = (new Date().getTime());
			 long now_temp;
			 long time_is_up = start + timeout_ms;
			 List<String> items_l = new ArrayList<String>();
			 items_l.addAll(request_paths_list);
			 
			 items_l.toArray();
			 do {
				 Har har = bmproxy.getHar();
				 
				 for (Iterator<HarEntry> it = har.getLog().getEntries().iterator(); it.hasNext();) {
					 
					 if (items_l.isEmpty())
						 break;
					 
					 HarEntry entry = it.next();
					 
		             try {

		            	 String httpmethod = entry.getRequest().getMethod().toLowerCase().trim();
		            	 if ("get".equals(httpmethod)){
		            		 
			            	 String url = entry.getRequest().getUrl().toLowerCase();
			            	 
			            	 //System.out.println("checking url = "+url); 

			            	 	for(String item:items_l) {
			            	 		//System.out.println("checking item = "+item); 
				            		 if (url.contains(item)) {
				            			 					            	 
						            		 items_l.remove(item);
						            		 har.getLog().getEntries().remove(entry);
						            		 System.out.println("found '"+item+"' in url = "+httpmethod + " "+url + " . "+items_l.size()+" to find "+ String.join(", ", items_l));
						            		 //System.out.println(entry.getMessage());
						            	 
					            		 break;
					            	 } 
				            	 }
		            	 }
		             }catch (Exception e) {
		            	 System.out.println("har iteration error: " + e+ " "+e.getMessage());
		             }
				 }
		         try {
						Thread.sleep(100);
						//System.out.println(" sleep 100");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		         now_temp = (new Date().getTime());
		         if (now_temp > time_is_up)
		        	 throw new TimeoutException("Request(s) ["+String.join(", ", items_l)+"] complete waiting was stopped due to timeout in "+timeout_ms+" ms");
			 }while( 	!	items_l.isEmpty() 	);
			 
	         try {
	        	 //for the ui render to complete
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		/*
		public static void WaitGETRequestsToComplete(WebDriver driver, List<String> request_paths_list, long timeout) throws TimeoutException{
			
			//System.out.println("Request(s) to wait ["+String.join(", ", items)+"]");
			 long start = (new Date().getTime());
			 long now_temp;
			 long time_is_up = start + timeout;
			 List<String> items_list = new ArrayList<String>();
			 items_list.addAll(request_paths_list);


			 items_list.toArray();
			 do {
				 for (Iterator<LogEntry> it = driver.manage().logs().get(LogType.PERFORMANCE).iterator(); it.hasNext();) {
					 if (items_list.isEmpty())
						 break;
		             LogEntry entry = it.next();
		             try {
			             JSONObject json = new JSONObject(entry.getMessage());
			             JSONObject message = json.getJSONObject("message");
			             String method = message.getString("method");
			             
			             if ("Network.responseReceived".equals(method)) {
			            	 JSONObject params = message.getJSONObject("params");
			            	 JSONObject response = params.getJSONObject("response");
			            	 String url = response.getString("url");

			            	 	for(String item:items_list) {
				            		 if (url.contains(item)) {
				            			 
						            	 String httpmethod="";
						            	 try {
						            		 httpmethod = params.getString("type");
						            	 }catch(org.json.JSONException e) {
						            		//continue;
						            	 }
						            	 if (httpmethod.toLowerCase().trim().equals("xhr"))
						            		 items_list.remove(item);
					            		 //System.out.println("found '"+item+"' in url = "+httpmethod + " "+url + " . "+items_l.size()+" to find");
					            		 //System.out.println(entry.getMessage());
					            		 break;
					            	 } 
				            	 }
			             }
		             }catch (Exception e) {
		            	 System.out.println(e);
		             }
				 }
		         try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		         now_temp = (new Date().getTime());
		         if (now_temp > time_is_up)
		        	 throw new TimeoutException("Request(s) ["+String.join(", ", items_list)+"] complete waiting was stopped due to timeout in "+timeout+" ms");
			 }while( 	!	items_list.isEmpty() 	);
			 
	         try {
	        	 //for the ui render to complete
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
		}*/
	}




}
