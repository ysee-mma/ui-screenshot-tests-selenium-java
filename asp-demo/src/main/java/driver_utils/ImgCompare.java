package driver_utils;


import java.awt.Color;
import java.awt.image.BufferedImage;

public class ImgCompare {
	

		
		public BufferedImage RawDiffImg;
		public int is_diff = -1; // -1 is unknown, 0  is no diff, 1 is they are different  
		
		
		/*
		 * img2 - is a baseline image
		 */
		ImgCompare(BufferedImage img1, BufferedImage img2){

			RawDiffImg = getDifferenceImage(img1, img2);
		}
		
		
		/*
		 * img2 - is a baseline image
		 */
		BufferedImage getDifferenceImage(BufferedImage img1, BufferedImage img2) {
		    // convert images to pixel arrays...
		    final int w = img1.getWidth(),
		            h = img1.getHeight(), 
		            highlight = Color.MAGENTA.getRGB();
		    final int[] p1 = img1.getRGB(0, 0, w, h, null, 0, w);
		    final int[] p2 = img2.getRGB(0, 0, w, h, null, 0, w);
		    // compare img1 to img2, pixel by pixel. If different, highlight img1's pixel...
		    for (int i = 0; i < p1.length; i++) {
		        if ((p1[i] != p2[i])
		        		&&(p2[i]!=Color.YELLOW.getRGB()))
		        {
		            p1[i] = highlight;
		            is_diff = 1;
		        }
		    }
		    // save img1's pixels to a new BufferedImage, and return it...
		    // (May require TYPE_INT_ARGB)
		    final BufferedImage out = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		    out.setRGB(0, 0, w, h, p1, 0, w);
		    return out;
		}

	
	
	

	
	

}
