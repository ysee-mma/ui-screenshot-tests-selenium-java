package driver_utils;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeOptions;

import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.filters.ResponseFilterAdapter;
import net.lightbody.bmp.proxy.CaptureType;
import Mocks.PageRequest;
import Mocks.ResponseFilterBuilder;

public class ProxySetter {

	
	
		public static void setProxy() {
			driver_wrapper.bmproxy = new BrowserMobProxyServer();
			
			driver_wrapper.bmproxy.setTrustAllServers(true);
			
			Set<CaptureType> capture = new HashSet<CaptureType>();
			capture.addAll(CaptureType.getAllContentCaptureTypes());
			capture.addAll(CaptureType.getHeaderCaptureTypes());
			capture.addAll(CaptureType.getCookieCaptureTypes());
			driver_wrapper.bmproxy.setHarCaptureTypes(capture);

			driver_wrapper.bmproxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
			
			driver_wrapper.bmproxy.start(driver_wrapper.port);
			
			driver_wrapper.bmproxy.newHar();

		}
	  
		public static ChromeOptions setSeleniumProxy(ChromeOptions options) {
			
			
	        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(driver_wrapper.bmproxy);
	        
	        seleniumProxy.setHttpProxy("127.0.0.1:" + driver_wrapper.port);
	        seleniumProxy.setSslProxy("127.0.0.1:" + driver_wrapper.port);

	        System.out.println("seleniumProxy.setSslProxy(\"127.0.0.1:\" + port);");
	        
	        System.out.println("bmproxy.addRequestFilter");

	        String proxyOption = "--proxy-server=" + seleniumProxy.getHttpProxy();
	        options.addArguments(proxyOption);
	        options.addArguments("--ignore-certificate-errors");
	        
	        System.out.println("options.addArguments(proxyOption);");


	        return options;
	        
		}  
		
		
		public static void ResponseFilterSetter (List<PageRequest>requests) {
			for(PageRequest req: requests) {
				driver_wrapper.bmproxy.addFirstHttpFilterFactory(new ResponseFilterAdapter.FilterSource(
						ResponseFilterBuilder.getMockResponse(req.json,req.path),
						10485760)); // 10 MB
			}
		}
}
