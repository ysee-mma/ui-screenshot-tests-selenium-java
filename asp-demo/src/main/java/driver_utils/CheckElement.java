package driver_utils;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

import PageObjectTypes.Component;

public class CheckElement {

	
	
	public static void Do (WebDriver driver, Component component, String testname) throws AssertionError {

		BufferedImage  fullImg1;
		
		if (component.getIgnore()!=null) {
			
			List<WebElement> web_el_to_ignore_list = new ArrayList<WebElement>();
			
			for(Component comp_to_ignore:component.getIgnore()) {
				try {
					web_el_to_ignore_list.add(driver.findElement(comp_to_ignore.getSelector()));
				}catch(Exception E) {
					
				}
			}
			fullImg1 = ScreenshotElement.cleared(driver.findElement(component.getSelector()),web_el_to_ignore_list);
		}
		else
			fullImg1 = ScreenshotElement.raw(driver.findElement(component.getSelector()));

		
			StartCheck(fullImg1, component.getName(), testname );

	}
	
	
	
	private static void StartCheck(BufferedImage  fullImg1, String componentName, String testname) {
		
		try {

			 File sc2 = new File(Tools.BaselinePath(componentName, testname));
			
			if ( ! sc2.exists()) {
				File parentDir = sc2.getParentFile();
				 if(parentDir !=null && ! parentDir.exists() ){
				    if(!parentDir.mkdirs()){
				        throw new IOException("error creating directories for "+sc2);
				    }
				 }

				ImageIO.write(
						fullImg1,
				        "png",
				        new File(Tools.BaselinePath(componentName, testname)));
				
				return;
	
			}
			BufferedImage  fullImg2 = ImageIO.read(sc2);
				
			try {
				ImgCompare diff = new ImgCompare(fullImg1, fullImg2);
			
				if (diff.is_diff == 1) {
					ImageIO.write(
							diff.RawDiffImg,
					        "png",
					        new File(   Tools.BaselineDIFFPath(componentName, testname)   ));
				
					ImageIO.write(
							fullImg1,
					        "png",
					        new File(  Tools.BaselineNEWPath(componentName, testname)  ));
					
					Assert.fail("There is a diff in the the element screenshot and the element baseline. See " +Tools.BaselineDIFFPath(componentName, testname));
				}
			}catch (IndexOutOfBoundsException e1) {
				Assert.fail("There is a difference in the the SIZE of baseline and actual pics. See " +Tools.BaselineNEWPath(componentName, testname));
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
