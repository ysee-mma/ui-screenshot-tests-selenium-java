package PageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;

import Mocks.suggestions;
import driver_utils.driver_wrapper;
import driver_utils.ProxySetter;
import Mocks.ResponseDataType;
import Mocks.PageRequest;
import PageObjectTypes.Component;



public class Bing_Help{
	
	public  String path = "https://help.bing.microsoft.com/";
	
	public Component Second_Dropdown = PageObjectTypes.Components_Lib.SecondDropdown();
	public Component Caption = PageObjectTypes.Components_Lib.Caption();
	public Component Search = PageObjectTypes.Components_Lib.Search();
	
	public Component page = new Component(
			By.cssSelector("div#apexbody"),
			null,
			Arrays.asList(Caption, Search),
			"bing help"
			);

	
	public suggestions suggestions = new suggestions();
	
	
	/*************************************
	 * All the requests in one list
	 ************************************/
	public List<Mocks.PageRequest> requests(){
		
		List<PageRequest> all = new ArrayList<PageRequest>();
		all.add(suggestions.suggestions_json);
		return all;
		}
	
	
	/**************************************
	 * Set Mock mode to all requests
	 ************************************/
	public void setMockForAllRequests(ResponseDataType type) {
		suggestions.setMockForAllRequests(type);
	}
	
	public void Open() {

		driver_wrapper.GoAndWait(path, requests());

	}
	
	public  void OpenMocked() {

		ProxySetter.ResponseFilterSetter(requests());
		
		Open();
		
	}
}


