package PageObjectTypes;


import org.openqa.selenium.By;

public class Components_Lib{

	public static Component SecondDropdown () {
		return new Component(
							By.xpath("//ul/li[@class='navbar-left dropdown'][2]"),
							//By.cssSelector("li:nth-child(2)"),
							null,
							null,
							"Dropdown");
	}
	
	public static Component Caption () {
		return new Component(
							By.cssSelector("h1"),
							null,
							null,
							"Caption");
	}
	public static Component Search () {
		return new Component(
							By.cssSelector("div#apex_search"),
							null,
							null,
							"Search");
	}

	
}

