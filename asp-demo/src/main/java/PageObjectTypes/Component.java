package PageObjectTypes;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import driver_utils.CheckElement;

public class Component {
	
	private By selector;
	
	private List<Component> childs;
	private List<Component> ignore;
	
	
	private String name_def;

	
	
	
	public Component(By selector, List<Component> children, List<Component> ignore, String name ){
		setSelector(selector);
		setChilds(children);
		setIgnore(ignore);
		setName(name);
	}
	
	public Component get() {
		return this;
	}
	
	
	public void setSelector(By by){
		selector = by;
	}
	public By getSelector(){
		return selector;
	}

	
	public void setChilds(List<Component> ch){
		if (ch != null)		{
			childs = new ArrayList<Component>();
			childs.addAll(ch);
		}
	}
	public List<Component> getChilds(){
		return childs;
	}
	
	
	public void setIgnore(List<Component> ig){
		if (ig != null)		{
			ignore = new ArrayList<Component>();
			ignore.addAll(ig);
		}
	}
	public List<Component> getIgnore(){
		return ignore;
	}
	

	
	public void setName(String name){
		name_def = name;
	}
	public String getName(){
		return name_def;
	}
	
	public void Check(WebDriver driver, String testname) throws AssertionError {
		try {
			if (childs!=null)
				for(Component comp:childs) {
					WebDriverWait wait = new WebDriverWait(driver,5);
					wait.until(ExpectedConditions.visibilityOfElementLocated(comp.selector));
					Thread.sleep(10000); // because the bell is a blinking element and it stops blinking in about 10 sec
				}
		}catch(Exception E) {};

		CheckElement.Do(driver, this, testname);
	}
	
	
	public void Click(WebDriver driver) {
		driver.findElement(selector).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
